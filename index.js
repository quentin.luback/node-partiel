const express = require("express");
const app = express();
const port = process.env.PORT || 3000;

// Moteur de templates EJS
app.set('view engine', 'ejs');

app.get("/",  function(req, res) {
  res.render('index');
});

app.post('/', function (req, res) {
  res.send('Bonjour !');
});

app.post("/text",  function(req, res) {
  res.send('test');
});

app.listen(port, function () {
  console.log('Example app listening on port : ', port)
})